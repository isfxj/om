const env = require('./upload.config.js'); //配置文件，在这文件里配置你的OSS keyId和KeySecret,timeout:87600;
const uploadFile = require('./uploadCommon.js'); //配置文件，在这文件里配置你的OSS keyId和KeySecret,timeout:87600;

const chooseImg = function (count, url="advert/",successc, failc) {
	const aliyunServerURL = env.uploadImageUrl;//OSS地址，需要https
	uni.chooseImage({
		// ablum相册 camera相机 ，默认二者都有
		sourceType: ['album', 'camera'],
		// orginal原图   compressed压缩图，默认二者都有
		sizeType: ['compressed'],
		// 最多可选图片张数，默认9
		count,
		// 成功则返回图片的本地文件路径列表 tempFilePaths
		success: res => {
			uni.showLoading({
				title:"上传中"
			});
			console.log(res);
			res.tempFiles.forEach(item=>{
				if(item.size <= 2097152){
					uploadFile(item.path,url,result => {
						// #ifdef H5
						result = result.replace('/oss','阿里云链接');
						// #endif
						successc(result);
						//成功返回
						uni.hideLoading()
						}, fail => { //失败返回
							console.log(JSON.stringify(fail));
							uni.hideLoading()
						}
					)
				}else{
					this.$tip.toast('上传图片大小不能超过2m')
				}
			})
		},
		// 接口调用失败的回调函数
		fail: err => {}
	});
};

module.exports = chooseImg;
// module.exports = chooseVideo;